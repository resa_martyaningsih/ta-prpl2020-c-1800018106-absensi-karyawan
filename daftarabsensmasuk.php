<!DOCTYPE html>
<html>
<head>
  <title>Halaman Admin</title>
  <link rel="stylesheet" type="text/css" href="home.css">
  <meta name='viewport' content='width=device-width, initial-scale=1'>
<script src='https://kit.fontawesome.com/a076d05399.js'></script>
  <link rel="stylesheet" type="text/css" href="menu.css" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
</head>
<body>
  <div class="content">
      <div class="left" align="center">
        <font size="5" color="white"><b>AdminWEB</b></font>
      </div>
      <div class="right" align="right">
        <a href="absensi.php" class="w3-bar-item w3-button" color="black"><i class='fas fa-user-alt' style='font-size:24px'></i></a>   
    </div> 

    <div class="header">
      <div class="kiri">

          <p style="background-color: #B0C4DE" align="center"><b>MENU</b></p>

         <div id='menu_ver'>
          <ul>
            <li><a href="home.php"><i class='fas fa-home' style='font-size:24px'></i><span> Home</span></a></li>
             <li><a href="karyawan.php"><i class='fas fa-address-card' style='font-size:24px'></i>   Karyawan</a></li>
             <li><a href="daftarabsensmasuk.php"><i class='fas fa-list-alt' style='font-size:24px'></i>   Absensi Masuk</a></li>   
             <li><a href="daftarabsenpulang.php"><i class='fas fa-list-alt' style='font-size:24px'></i>   Absensi Pulang</a></li> 
             <li><a href="logout.php"><i class='fas fa-sign-out-alt' style='font-size:24px'></i>    Keluar</a></li>
          </ul>
          </div>  
      </div>
      <div class="kanan">
        <h1><b>ABSENSI KARYAWAN</b></h1>
        <div class="container">            
          <table class="table table-striped">
            <?php
              if (isset($_GET['cari'])) {
                $cari = $_GET['cari'];
                echo "<b>Hasil Pencarian : ".$cari."</b>";
              }
            ?>
            <thead>
              <tr>
                <th>NO</th>
                <th>ID</th>
                <th>Waktu Absensi Masuk</th>
              </tr>
              <?php 
                include 'connection.php';
                $no = 1;
                $data = mysqli_query($mysqli,"select * from post");
                while($d = mysqli_fetch_array($data)){
                  ?>
                  <tr>
                    <td><?php echo $no++; ?></td>
                    <td><?php echo $d['id']; ?></td>
                    <td><?php echo $d['tanggal']; ?></td>
                    
                  </tr>
                  <?php 
                }
                ?>
          </table>
        </div>

      </div>
    </div> 
</nav>
</body>
</html>
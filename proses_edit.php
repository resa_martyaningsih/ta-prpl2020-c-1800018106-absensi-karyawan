<?php

include 'koneksi.php';
$id = $_GET['id'];
$nama = mysqli_query($koneksi, "SELECT *FROM id WHERE id = '$id'");

?>


<!DOCTYPE html>
<html lang="en">
<head>
  <title>MRTYN Bakery</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>
<body>

<div class="container">
  <h2>Update Data Karyawan</h2>
  <form action="edit.php" method="POST">
    <div class="form-group">
      <label for="pwd">Id Karyawan:</label>
      <input type="text" class="form-control" id="pwd" placeholder="Masukkan Id Pegawai" value="<?php echo $nama ['id']?>" name="id">
    </div>
    <div class="form-group">
      <label for="pwd">Nama Karyawan:</label>
      <input type="text" class="form-control" id="pwd" placeholder="Masukkan Nama Karyawan" value="<?php echo $nama ['nama']?>" name="nama">
    </div>
    <div class="form-group form-check">
      <label class="form-check-label">
        <input class="form-check-input" type="checkbox" name="remember"> Remember me
      </label>
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
</div>

</body>
</html>

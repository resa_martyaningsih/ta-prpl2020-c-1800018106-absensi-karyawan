<!DOCTYPE html>
<html>
<head>
  <title>Halaman Admin</title>
  <link rel="stylesheet" type="text/css" href="home.css">
  <meta name='viewport' content='width=device-width, initial-scale=1'>
<script src='https://kit.fontawesome.com/a076d05399.js'></script>
  <link rel="stylesheet" type="text/css" href="menu.css" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
</head>
<body>
  <div class="content">
      <div class="left" align="center">
        <font size="5" color="white"><b>AdminWEB</b></font>
      </div>
      <div class="right" align="right">
        <a href="absensi.php" class="w3-bar-item w3-button" color="black"><i class='fas fa-user-alt' style='font-size:24px'></i></a>   
    </div> 

    <div class="header">
      <div class="kiri">

          <p style="background-color: #B0C4DE" align="center"><b>MENU</b></p>

         <div id='menu_ver'>
          <ul>
            <li><a href="home.php"><i class='fas fa-home' style='font-size:24px'></i><span> Home</span></a></li>
             <li><a href="karyawan.php"><i class='fas fa-address-card' style='font-size:24px'></i>   Karyawan</a></li>
             <li><a href="daftarabsensmasuk.php"><i class='fas fa-list-alt' style='font-size:24px'></i>   Absensi Masuk</a></li>   
             <li><a href="daftarabsenpulang.php"><i class='fas fa-list-alt' style='font-size:24px'></i>   Absensi Pulang</a></li> 
             <li><a href="logout.php"><i class='fas fa-sign-out-alt' style='font-size:24px'></i>    Keluar</a></li>
          </ul>
          </div>  
      </div>
      <div class="kanan">
        <h1><b>DASHBOARD</b></h1>
        <div class="isi">
          <div class="alert alert-success">
            <strong>Success!</strong> This alert box could indicate a successful or positive action.
          </div>
          <div class="card bg-success text-white">
            <div class="card-body"><div class="times">
  <script type="text/javascript">        
    function tampilkanwaktu(){   
    var waktu = new Date();        
    var sh = waktu.getHours() + "";    
    var sm = waktu.getMinutes() + "";     
    var ss = waktu.getSeconds() + "";  
    document.getElementById("clock").innerHTML = (sh.length==1?"0"+sh:sh) + ":" + (sm.length==1?"0"+sm:sm) + ":" + (ss.length==1?"0"+ss:ss);
    }
</script>
<body onload="tampilkanwaktu();setInterval('tampilkanwaktu()', 1000);">        
<span id="clock"></span> 

<?php
$hari = date('l');

if ($hari=="Sunday") {
 echo "Minggu";
}elseif ($hari=="Monday") {
 echo "Senin";
}elseif ($hari=="Tuesday") {
 echo "Selasa";
}elseif ($hari=="Wednesday") {
 echo "Rabu";
}elseif ($hari=="Thursday") {
 echo("Kamis");
}elseif ($hari=="Friday") {
 echo "Jum'at";
}elseif ($hari=="Saturday") {
 echo "Sabtu";
}
?>,
<?php
  $tgl =date('d');
  echo $tgl;
  $bulan =date('F');
  if ($bulan=="January") {
   echo " Januari ";
  }elseif ($bulan=="February") {
   echo " Februari ";
  }elseif ($bulan=="March") {
   echo " Maret ";
  }elseif ($bulan=="April") {
   echo " April ";
  }elseif ($bulan=="May") {
   echo " Mei ";
  }elseif ($bulan=="June") {
   echo " Juni ";
  }elseif ($bulan=="July") {
   echo " Juli ";
  }elseif ($bulan=="August") {
   echo " Agustus ";
  }elseif ($bulan=="September") {
   echo " September ";
  }elseif ($bulan=="October") {
   echo " Oktober ";
  }elseif ($bulan=="November") {
   echo " November ";
  }elseif ($bulan=="December") {
   echo " Desember ";
  }
  $tahun=date('Y');
    echo $tahun;
?>
  </div>
</div></div>
          </div>
          <br>
          <div class="card bg-info text-white">
            <div class="card-body">
                <h2>Jumlah Karyawan 7</h2>
                <i class='fas fa-users' style='font-size:48px;color: #E6E6FA' align="right"></i>
            </div>
          </div>
          <br>
          <div class="jumbotron">
            <h3><i class='far fa-comments'></i>Info</h3> 
            <p>Ini merupakan halaman admin pengelola karyawan dan absen</p> 
          </div>
        </div>

      </div>
    </div> 
</nav>
</body>
</html>
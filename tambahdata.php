<!DOCTYPE html>
<html lang="en">
<head>
  <title>MRTYN Bakery</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
</head>
<body>

<div class="container">
  <h2>Tambah Data Karyawan</h2>
  <form action="proses_input.php" method="POST">
    <div class="form-group">
      <label for="id">Id Karyawan:</label>
      <input type="text" class="form-control" id="pwd" placeholder="Masukkan Id Karyawan" name="id">
    </div>
    <div class="form-group">
      <label for="pwd">Nama Karyawan:</label>
      <input type="text" class="form-control" id="pwd" placeholder="Masukkan Nama Karyawan" name="nama">
    </div>
    <div class="form-group">
      <label for="pwd">Pekerjaan :</label>
      <input type="text" class="form-control" id="pwd" placeholder="Masukkan Pekerjaan" name="pekerjaan">
    </div>
    <div class="form-group">
      <label for="pwd">Nomor Telefon:</label>
      <input type="pwd" class="form-control" id="pwd" placeholder="Masukkan Nomor Telefon" name="no_hp">
    </div>
    <div class="form-group">
      <label for="pwd">Alamat:</label>
      <input type="pwd" class="form-control" id="pwd" placeholder="Masukkan Alamat Pegawai" name="alamat">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
</div>

  
</body>
</html>
